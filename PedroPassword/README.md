# Welcome to genpasswd
This is a simple to for generating a password

## Installation

### Clone repository

```console
mkdir ~/Projects
cd ~/Projects
git clone https://gitlab.com/gantryyork/MentorCode.git
```

### Create Virtual Environment (optional)
```console
mkdir ~/Environments
python3 -m venv ~/Environments/password
source ~/Environments/bin/activate
```

### Install the generatepassword Package

```console
cd ~/Projects/MentorCode/PasswordPedro
pip install .
```


## Usage
After installation type
```console
genpasswd --help
```
for instructions