#!/usr/bin/env python3

# Imports: Standard Library
# -------------------------
from argparse import ArgumentParser
import random

# Constants
# ---------
ALPHA_SYMBOLS = list('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') 
NUMERIC_SYMBOLS = list('0123456789') 
SPECIAL_SYMBOLS = list('!@#$%^&*+-')

def main():

    args = parse_command_line()

    if args.numeric + args.special + args.alpha > args.length:
        print(f"ERROR: You must specify a length of at least {args.numberic + args.special + args.alpha}")
        exit(1)
    elif args.numeric + args.special > args.length:
        print(f"ERROR: password length is not long enough to accomodate {args.numeric} + {args.special} symbols")
        exit(2)

    raw_password = list()

    for _ in range(0,args.numeric):
        raw_password.append(random.choice(NUMERIC_SYMBOLS)) 
    for _ in range(0,args.special):
        raw_password.append(random.choice(SPECIAL_SYMBOLS))
    for _ in range(0, (args.length - args.numeric - args.special)):
        raw_password.append(random.choice(ALPHA_SYMBOLS))

    random.shuffle(raw_password)
    print(list_to_str(raw_password))

def list_to_str(list_in):
    str_out = ''.join(list_in)
    return str_out


def parse_command_line():
    parser = ArgumentParser(
        prog="genpassword",
        description="Generates a random password",
    )
    parser.add_argument(
        "-l", "--length",
        type=int,
        default=15,
        help="Length of password.  Default is 15."
    )
    parser.add_argument(
        "-a", "--alpha",
        type=int,
        default=1,
        help="Number of alpha symbols to include. Default is 1.  Will pad to length with alpha symbols"
    )
    parser.add_argument(
        "-n", "--numeric",
        type=int,
        default=1,
        help="Number of numeric symbols to include. Default is 1"
    )
    parser.add_argument(
        "-s", "--special",
        type=int,
        default=1,
        help="Number of special symbols to include. Default is 1"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
    exit()