# MentorCode

This is a repository with code examples that I've created to mentor others.


# Advice for beginners
1. Learn to edit code with vi before you use and IDE
1. Learn to use git by the command line before you use a GUI
1. Start with a simple project and learn what you need to know to complete the project rather than taking an extensive course
1. Learn by looking at other programmers code

# Advice for intermediates
1. Use virtual environments
1. Use setuptools and install with pip
1. Lint your code with autopep8 or black
1. Use docstrings




