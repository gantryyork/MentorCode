import random
from card import Card

SUITS = ["Clubs", "Spades", "Hearts", "Diamonds"]
NAMES = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"]



class Deck(object):

    def __init__(self):

        self.cards = list()

        for suit in SUITS:
            for name in NAMES:
                card = Card(name, suit)
                self.cards.append(card)


    def fan_deck(self, short=True):
        for card in self.cards:
                card.display(short)

    def deal(self, num_of_cards=1):
        return self.cards.pop()


    def shuffle(self):
        random.shuffle(self.cards)

