class Card:

    def __init__(self, name, suit):

        self.name = name
        self.suit = suit

    def display(self, short=True):
        if short:
            print(f"{self.name[:1]}{self.suit[:1]}")
        else:
            print(self.name, self.suit)


