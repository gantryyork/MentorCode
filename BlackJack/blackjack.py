#!/usr/bin/env python3

from deck import Deck
from card import Card


def main():

    deck = Deck()
    deck.shuffle()

    deck.deal().display()

    hit_me = True
    while hit_me and len(deck.cards) > 0:
        deck.deal().display()

        response = input("Do you want a hit?")
        if response in ["Y", "y", "yes"]:
            hit_me = True
        else:
            hit_me = False



if __name__ == "__main__":
    main()
    exit()